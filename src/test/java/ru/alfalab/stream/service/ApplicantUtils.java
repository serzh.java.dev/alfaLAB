package ru.alfalab.stream.service;

import ru.alfalab.stream.model.Applicant;

public class ApplicantUtils {
    public static Applicant getApplicant1() {
        return Applicant.builder()
                .name("Сергей")
                .name("Жуков")
                .gender("Мужчина")
                .profession("Java developer")
                .address("г.Москва")
                .build();
    }

    public static Applicant getApplicant2() {
        return Applicant.builder()
                .name("Сергей")
                .name("Петров")
                .gender("Мужчина")
                .profession("С++")
                .address("г.Пермь")
                .build();
    }
}
