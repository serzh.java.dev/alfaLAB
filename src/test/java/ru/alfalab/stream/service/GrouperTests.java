package ru.alfalab.stream.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import ru.alfalab.stream.model.Applicant;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(SpringRunner.class)
public class GrouperTests {
    private Grouper grouper;

    @Before
    public void setUp() {
        grouper = new GrouperImpl();
    }

    @Test
    public void testGroupByName() {
        List<Applicant> applicants = new ArrayList<>();
        applicants.add(ApplicantUtils.getApplicant1());
        applicants.add(ApplicantUtils.getApplicant2());

        Map<String, List<Applicant>> groupByName = grouper.groupByName(applicants);

        assertThat(groupByName, notNullValue());
        assertThat(groupByName.size(), is(2));
    }
}
