package ru.alfalab.hibernate.model;

public enum DocumentStatus {
        NONE,
        ACTIVE,
        ARCHIVE,
        DRAFT;
}
