package ru.alfalab.hibernate.model;

import javax.persistence.*;

import lombok.*;

import java.util.UUID;

@Entity
@Getter
@Table(name = "`DOCUMENT`")
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Document {

    @Setter
    @Id
    @GeneratedValue
    @Column(name = "`ID`")
    UUID id;

    @Column(name = "`NUMBER`")
    String number;

    @Column(name = "`TYPE`")
    String type;

    @Column(name = "`STATUS`")
    @Enumerated(EnumType.STRING)
    DocumentStatus status;

    @ToString.Exclude
    @Setter
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "`HUMAN_ID`")
    Human human;
}
