package ru.alfalab.hibernate.model;

import lombok.*;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;


@Entity
@Table(name = "`HUMAN`")
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Human {
    @Setter
    @Id
    @GeneratedValue
    @Column(name = "`ID`")
    UUID id;

    @Column(name = "`NAME`")
    String name;

    @Column(name = "`LAST_NAME`")
    String lastName;

    @Column(name = "`SUR_NAME`")
    String surName;

    @Column(name = "`AGE`")
    Integer age;

    @Column(name = "`GENDER`")
    String gender;

    @OneToMany(mappedBy = "human", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    List<Document> documents;

    public void setDocuments(List<Document> documents) {
        if (!CollectionUtils.isEmpty(documents)) {
            documents.forEach(document ->
                    document.setHuman(this));
        }

        this.documents = documents;
    }

    public void updateDocuments() {
        if (!CollectionUtils.isEmpty(this.documents)) {
            documents.forEach(document ->
                    document.setHuman(this));
        }
    }
}
