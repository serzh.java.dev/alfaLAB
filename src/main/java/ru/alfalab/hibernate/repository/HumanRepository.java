package ru.alfalab.hibernate.repository;

import org.springframework.data.jpa.repository.Query;
import ru.alfalab.hibernate.controller.dto.HumanViewDto;
import ru.alfalab.hibernate.model.Human;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface HumanRepository extends JpaRepository<Human, UUID> {
    /*native
    SELECT (h."LAST_NAME"  || ' ' || h."NAME" || ' ' || h."SUR_NAME") as "FULLNAME", d."TYPE" as "DOCUMENT_TYPE", d."NUMBER"  as "DOCUMENT_NUMBER"
    FROM "HUMAN" h  LEFT JOIN "DOCUMENT" d
    ON h."ID"  = d."HUMAN_ID"
    WHERE
       d."STATUS"  = 'ACTIVE'
      AND d."NUMBER"  LIKE '%777%'
     */
    @Query(value = "SELECT new ru.alfalab.hibernate.controller.dto.HumanViewDto( CONCAT(h.lastName, ' ', h.name, ' ', h.surName), d.number, d.type ) " +
            "FROM Human h LEFT JOIN Document d " +
            "ON h.id = d.human.id " +
            "WHERE " +
            "d.status = 'ACTIVE'" +
            "AND d.number LIKE '%777%'")
    List<HumanViewDto> findByDocumentActiveAnd777();


}
