package ru.alfalab.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.alfalab.hibernate.model.Document;

import java.util.UUID;

public interface DocumentRepository extends JpaRepository<Document, UUID> {
}
