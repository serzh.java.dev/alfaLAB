package ru.alfalab.hibernate.service;

import ru.alfalab.hibernate.controller.dto.HumanDto;
import ru.alfalab.hibernate.controller.dto.HumanViewDto;
import ru.alfalab.hibernate.model.Human;

import java.util.List;

public interface HumanService {
    List<HumanViewDto> getByDocuments();

    Human create(HumanDto dto);
}
