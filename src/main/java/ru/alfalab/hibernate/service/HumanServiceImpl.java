package ru.alfalab.hibernate.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import ru.alfalab.hibernate.controller.dto.HumanDto;
import ru.alfalab.hibernate.controller.dto.HumanViewDto;
import ru.alfalab.hibernate.mapper.HumanMapper;
import ru.alfalab.hibernate.model.Human;
import ru.alfalab.hibernate.repository.HumanRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class HumanServiceImpl implements HumanService {
    private final HumanRepository repository;
    private final HumanMapper mapper;

    @Override
    @Transactional
    @EventListener(ApplicationReadyEvent.class)
    public List<HumanViewDto> getByDocuments() {
        List<HumanViewDto> humans = repository.findByDocumentActiveAnd777();

        log.info("Method execution getByDocuments");
        humans.stream()
                .map(HumanViewDto::toString)
                .forEach(log::info);

        return humans;
    }

    @Override
    @Transactional
    public Human create(HumanDto dto) {
        Human human = mapper.toEntity(dto);
        human.updateDocuments();

        return repository.save(human);
    }
}
