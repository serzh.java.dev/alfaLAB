package ru.alfalab.hibernate.mapper;

import org.mapstruct.Mapper;
import ru.alfalab.hibernate.controller.dto.HumanDto;
import ru.alfalab.hibernate.model.Human;

@Mapper(uses = {
        DocumentMapper.class})
public interface HumanMapper {
    Human toEntity(HumanDto dto);
}
