package ru.alfalab.hibernate.mapper;

import org.mapstruct.Mapper;
import ru.alfalab.hibernate.controller.dto.DocumentDto;
import ru.alfalab.hibernate.model.Document;

@Mapper
public interface DocumentMapper {
    Document toEntity(DocumentDto dto);
}
