package ru.alfalab.hibernate.controller;

public interface ControllerConstants {
    String HUMAN = "/human";

    String PREFIX = "/api";
}
