package ru.alfalab.hibernate.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.alfalab.hibernate.controller.dto.HumanDto;
import ru.alfalab.hibernate.controller.dto.HumanViewDto;
import ru.alfalab.hibernate.model.Human;
import ru.alfalab.hibernate.service.HumanService;

import java.util.List;

import static ru.alfalab.hibernate.controller.ControllerConstants.HUMAN;
import static ru.alfalab.hibernate.controller.ControllerConstants.PREFIX;

@RestController
@RequestMapping(PREFIX + HUMAN)
@RequiredArgsConstructor
public class HumanController {
    private final HumanService service;

    @PostMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Human> create(@RequestBody HumanDto dto) {

        return new ResponseEntity<>(service.create(dto), HttpStatus.CREATED);
    }

    @GetMapping(value = "/find/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<HumanViewDto> getByDocuments() {
        return service.getByDocuments();
    }
}
