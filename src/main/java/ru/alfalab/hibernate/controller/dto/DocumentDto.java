package ru.alfalab.hibernate.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.alfalab.hibernate.model.DocumentStatus;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class DocumentDto {
    String number;

    String type;

    DocumentStatus status;
}
