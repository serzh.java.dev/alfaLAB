package ru.alfalab.hibernate.controller.dto;

import lombok.Value;

@Value
public class HumanViewDto {
    String fullName;
    String numberDocument;
    String typeDocument;
}
