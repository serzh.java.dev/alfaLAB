package ru.alfalab.hibernate.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class HumanDto {
    String name;

    String lastName;

    String surName;

    Integer age;

    String gender;

    List<DocumentDto> documents;
}
