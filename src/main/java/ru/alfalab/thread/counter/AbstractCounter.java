package ru.alfalab.thread.counter;

import ru.alfalab.thread.file.FileHandler;

import java.io.IOException;
import java.util.concurrent.locks.Lock;

public abstract class AbstractCounter {
    private long value;

    private final FileHandler fileHandler;

    protected AbstractCounter(FileHandler fileHandler) {
        this.fileHandler = fileHandler;
    }


    public final long getValue() throws IOException {
        final Lock lock = this.getReadLock();

        lock.lock();
        try {
            this.value = fileHandler.getReadValue();

            return this.value;
        } finally {
            lock.unlock();
        }
    }

    public final void increment() throws IOException {
        final Lock lock = this.getWriteLock();

        lock.lock();
        try {
            this.value++;

            fileHandler.write(value);
        } finally {
            lock.unlock();
        }
    }

    protected abstract Lock getReadLock();

    protected abstract Lock getWriteLock();
}
