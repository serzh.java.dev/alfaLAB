package ru.alfalab.thread.file;

import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;

public class FileHandler {
    private static final String FILE_NAME = "result.txt";

    public Long getReadValue() throws IOException {
        byte[] bytes = readLastMaxLong("result.txt");

        return getLastValue(new String(bytes));
    }

    public void write(Long value) throws IOException {
        writeFile(FILE_NAME, value);
        writeFile(Thread.currentThread().getName() + ".txt", value);
    }

    private byte[] readLastMaxLong(String filePath) throws IOException {
        RandomAccessFile file = new RandomAccessFile(filePath, "r");


        int size = String.valueOf(Long.MAX_VALUE).length();
        if (file.length() == 0) {
            return new byte[]{};
        }

        long position = file.length() - size;
        if (position < 0) {
            file.seek(0);
            size = (int) file.length();
        } else {
            file.seek(position);

        }

        byte[] bytes = new byte[size];
        file.read(bytes);
        file.close();
        return bytes;
    }


    private Long getLastValue(String string) {
        if (string.isEmpty()) {
            return 1L;
        }

        String[] massValue = string.split(" ");

        return Long.parseLong(massValue[massValue.length - 1]);
    }

    private void writeFile(String fileName, long value) throws IOException {
        FileWriter writer = new FileWriter(fileName, true);
        writer.write(value + " ");
        writer.flush();
    }
}
