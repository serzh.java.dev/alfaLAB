package ru.alfalab.thread;

import lombok.extern.slf4j.Slf4j;
import ru.alfalab.thread.counter.AbstractCounter;

import java.io.IOException;
import java.time.Duration;
import java.time.ZonedDateTime;

@Slf4j
public class ReadingValueTask implements Runnable {
    private static final long MAX_VALUE = 1000000L;

    private final AbstractCounter abstractCounter;

    public ReadingValueTask(AbstractCounter abstractCounter) {
        this.abstractCounter = abstractCounter;
    }

    @Override
    public void run() {
        ZonedDateTime start = ZonedDateTime.now();
        log.info("Starting a thread: " + Thread.currentThread().getName());
        boolean toContinue = true;

        while (toContinue) {
            toContinue = incrementValue();
        }
        log.info("Thread termination: " + Thread.currentThread().getName());
        ZonedDateTime stop = ZonedDateTime.now();
        Duration between = Duration.between(start, stop);
        log.info("Thread work time: " + between.getSeconds());

    }

    private boolean incrementValue() {
        try {
            long value = abstractCounter.getValue();

            if (value >= MAX_VALUE) {
                return false;
            }

            abstractCounter.increment();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }
}
