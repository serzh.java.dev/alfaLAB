package ru.alfalab.thread;

import lombok.extern.slf4j.Slf4j;
import ru.alfalab.thread.counter.CounterByReadWriteLock;
import ru.alfalab.thread.file.FileHandler;

@Slf4j
public class MainThread {
    public static void main(String[] args) throws Exception {
        CounterByReadWriteLock readWriteLock = new CounterByReadWriteLock(new FileHandler());
        Thread t1 = new Thread(new ReadingValueTask(readWriteLock));
        Thread t2 = new Thread(new ReadingValueTask(readWriteLock));
        t1.start();
        t2.start();
    }
}
