package ru.alfalab.stream.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Applicant implements NamedObject {
    String name;
    String surname;
    String gender;
    String profession;
    String address;

    @Override
    public String getName() {
        return name;
    }
}
