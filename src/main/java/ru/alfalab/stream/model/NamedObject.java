package ru.alfalab.stream.model;

public interface NamedObject {
    String getName();
}
