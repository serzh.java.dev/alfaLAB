package ru.alfalab.stream.service;

import ru.alfalab.stream.model.Applicant;

import java.util.List;
import java.util.Map;

public interface Grouper {
    Map<String, List<Applicant>> groupByName(List<Applicant> applicants);
}
