package ru.alfalab.stream.service;

import org.springframework.stereotype.Service;
import ru.alfalab.stream.model.Applicant;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class GrouperImpl implements Grouper {
    @Override
    public Map<String, List<Applicant>> groupByName(List<Applicant> applicants) {
        return applicants.stream()
                .collect(Collectors.groupingBy(Applicant::getName));
    }
}
